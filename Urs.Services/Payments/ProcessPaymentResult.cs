﻿using System.Collections.Generic;
using Urs.Data.Domain.Payments;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial class ProcessPaymentResult
    {
        private PaymentStatus _newPaymentStatus = PaymentStatus.Pending;
        private OrderStatus _newOrderStatus = OrderStatus.Pending;
        public IList<string> Errors { get; set; }

        public ProcessPaymentResult() 
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get { return (this.Errors.Count == 0); }
        }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }

        public PaymentStatus NewPaymentStatus
        {
            get
            {
                return _newPaymentStatus;
            }
            set
            {
                _newPaymentStatus = value;
            }
        }

        public OrderStatus NewOrderStatus
        {
            get
            {
                return _newOrderStatus;
            }
            set
            {
                _newOrderStatus = value;
            }
        }
    }
}
