namespace Urs.Services.Payments
{
    public enum PaymentMethodType : int
    {
        Unknown = 0,
        Standard = 10,
        Redirection = 15,
        Button = 20,
    }
}
