using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public static class CategoryExtensions
    {
        public static IList<Category> SortCategoriesForTree(this IList<Category> source, int parentId = 0, bool ignoreCategoriesWithoutExistingParent = false)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            var result = new List<Category>();

            foreach (var cat in source.ToList().FindAll(c => c.ParentCategoryId == parentId))
            {
                result.Add(cat);
                result.AddRange(SortCategoriesForTree(source, cat.Id, true));
            }
            if (!ignoreCategoriesWithoutExistingParent && result.Count != source.Count)
            {
                foreach (var cat in source)
                    if (result.Where(x => x.Id == cat.Id).FirstOrDefault() == null)
                        result.Add(cat);
            }
            return result;
        }

        public static GoodsCategory FindGoodsCategory(this IList<GoodsCategory> source,
            int goodsId, int categoryId)
        {
            foreach (var goodsCategory in source)
                if (goodsCategory.GoodsId == goodsId && goodsCategory.CategoryId == categoryId)
                    return goodsCategory;

            return null;
        }

        public static string GetCategoryNameWithPrefix(this Category category, ICategoryService categoryService)
        {
            string result = string.Empty;

            while (category != null)
            {
                if (String.IsNullOrEmpty(result))
                    result = category.Name;
                else
                    result = "--" + result;
                category = categoryService.GetCategoryById(category.ParentCategoryId);
            }
            return result;
        }


        public static string GetCategoryBreadCrumb(this Category category, ICategoryService categoryService)
        {
            string result = string.Empty;

            while (category != null && !category.Deleted)
            {
                if (String.IsNullOrEmpty(result))
                    result = category.Name;
                else
                    result = category.Name + " >> " + result;

                category = categoryService.GetCategoryById(category.ParentCategoryId);

            }
            return result;
        }

    }
}
