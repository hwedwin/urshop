﻿using System.Net;
using Urs.Data.Domain.Configuration;
using Urs.Services.Tasks;

namespace Urs.Services.Common
{
    public partial class KeepAliveTask : IScheduleTask
    {
        private readonly StoreInformationSettings _storeInformationSettings;
        public KeepAliveTask(StoreInformationSettings storeInformationSettings)
        {
            this._storeInformationSettings = storeInformationSettings;
        }
        public void Execute()
        {
            string url = _storeInformationSettings.StoreUrl + "keepalive/index";
            using (var wc = new WebClient())
            {
                wc.DownloadString(url);
            }
        }
    }
}
