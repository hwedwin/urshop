using Urs.Core;

namespace Urs.Services.Common
{
    public partial interface IMaintenanceService
    {
        int? GetTableIdent<T>() where T : BaseEntity;

        void SetTableIdent<T>(int ident) where T : BaseEntity;
    }
}
