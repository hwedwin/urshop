
using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Core.Data;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Configuration;
using Urs.Services.Users;
using Urs.Services.Media;
using Urs.Services.Plugins;

namespace Urs.Services.Authentication.External
{
    public partial class OpenAuthenticationService : IOpenAuthenticationService
    {
        private readonly IUserService _userService;
        private readonly IPictureService _pictureService;
        private readonly IPluginFinder _pluginFinder;
        private readonly ExternalAuthSettings _externalAuthenticationSettings;
        private readonly IRepository<ExternalAuth> _externalAuthenticationRecordRepository;

        public OpenAuthenticationService(
            IPictureService pictureService,
            IRepository<ExternalAuth> externalAuthenticationRecordRepository,
            IPluginFinder pluginFinder,
            ExternalAuthSettings externalAuthenticationSettings,
            IUserService userService)
        {
            this._pictureService = pictureService;
            this._externalAuthenticationRecordRepository = externalAuthenticationRecordRepository;
            this._pluginFinder = pluginFinder;
            this._externalAuthenticationSettings = externalAuthenticationSettings;
            this._userService = userService;
        }

        public virtual IList<IExternalAuthenticationMethod> LoadActiveExternalAuthenticationMethods()
        {
            return LoadAllExternalAuthenticationMethods()
                   .Where(provider => _externalAuthenticationSettings.ActiveAuthenticationMethodSystemNames.Contains(provider.PluginDescriptor.SystemName, StringComparer.InvariantCultureIgnoreCase))
                   .ToList();
        }

        public virtual IExternalAuthenticationMethod LoadExternalAuthenticationMethodBySystemName(string systemName)
        {
            var descriptor = _pluginFinder.GetPluginDescriptorBySystemName<IExternalAuthenticationMethod>(systemName);
            if (descriptor != null)
                return descriptor.Instance<IExternalAuthenticationMethod>();

            return null;
        }

        public virtual IList<IExternalAuthenticationMethod> LoadAllExternalAuthenticationMethods()
        {
            return _pluginFinder.GetPlugins<IExternalAuthenticationMethod>().ToList();
        }

        public virtual void AssociateExternalAccountWithUser(User user, OpenAuthParameters parameters)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var externalAuthenticationRecord = new ExternalAuth()
            {
                UserId = user.Id,
                Name = parameters.UserClaim?.Nickname,
                AvatarUrl = parameters.UserClaim?.HeadImgUrl,
                UnionId = parameters.UnionId,
                OpenId = parameters.OpenId,
                OAuthToken = parameters.OAuthToken,
                OAuthRefreshToken = parameters.OAuthRefreshToken,
                ProviderSystemName = parameters.ProviderSystemName,
            };
            _externalAuthenticationRecordRepository.Insert(externalAuthenticationRecord);

            if (parameters.UserClaim == null) return;

            user.Nickname = parameters.UserClaim?.Nickname;
            if (!string.IsNullOrEmpty(parameters.UserClaim?.HeadImgUrl))
                user.AvatarPictureId = _pictureService.DownloadPictureUrl(parameters.UserClaim?.HeadImgUrl);

            _userService.UpdateUser(user);
        }

        public virtual bool AccountExists(OpenAuthParameters parameters)
        {
            return GetUser(parameters) != null;
        }

        public virtual User GetUser(OpenAuthParameters parameters)
        {
            var record = _externalAuthenticationRecordRepository.Table
                .Where(o => o.OpenId == parameters.OpenId && o.ProviderSystemName == parameters.ProviderSystemName)
                .FirstOrDefault();

            if (record != null)
                return _userService.GetUserById(record.UserId);

            return null;
        }

        public virtual IList<ExternalAuth> GetExternalIdentifiersFor(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return user.ExternalAuthRecords.ToList();
        }

        public virtual void RemoveAssociation(OpenAuthParameters parameters)
        {
            var record = _externalAuthenticationRecordRepository.Table
                .Where(o => o.OpenId == parameters.OpenId && o.ProviderSystemName == parameters.ProviderSystemName)
                .FirstOrDefault();

            if (record != null)
                _externalAuthenticationRecordRepository.Delete(record);
        }

        public virtual void DeleteExternalAuthenticationRecord(ExternalAuth record)
        {
            if (record == null)
                throw new ArgumentNullException("record");
            _externalAuthenticationRecordRepository.Delete(record);
        }

        public virtual void UpdateExternalAuthenticationRecord(ExternalAuth record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            _externalAuthenticationRecordRepository.Update(record);
        }

        public virtual void InsertExternalAuthenticationRecord(ExternalAuth record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            _externalAuthenticationRecordRepository.Insert(record);
        }
        public virtual ExternalAuth GetExternalAuthentication(OpenAuthParameters parameters)
        {
            var record = _externalAuthenticationRecordRepository.Table
                .Where(o => o.OpenId == parameters.OpenId && o.ProviderSystemName == parameters.ProviderSystemName)
                .FirstOrDefault();
            return record;
        }

        public virtual ExternalAuth GetExternalAuthenticationRecordByOpenId(string openid)
        {
            if (string.IsNullOrWhiteSpace(openid))
                return null;

            var query = _externalAuthenticationRecordRepository.Table;
            query = query.Where(q => q.OAuthRefreshToken == openid);

            return query.FirstOrDefault();
        }
    }
}