﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace Urs.Web.Infrastructure
{
    public static class ValidateHelper
    {
        public static string FirstMessage(this ModelStateDictionary modelState)
        {
            dynamic errors = new JObject();
            foreach (var key in modelState.Keys)
            {
                var state = modelState[key];
                if (state.Errors.Any())
                {
                    return Convert.ToString(state.Errors.First().ErrorMessage);
                    //errors[key] = state.Errors.First().ErrorMessage;
                }
            }

            return Convert.ToString(errors);
        }
    }
}