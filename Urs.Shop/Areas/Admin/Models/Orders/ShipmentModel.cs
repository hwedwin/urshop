﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Urs.Admin.Models.Common;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Orders
{
    public partial class ShipmentModel : BaseEntityModel
    {
        public ShipmentModel()
        {
            this.Goodss = new List<ShipmentOrderGoodsModel>();
            this.AvailableExpressNames = new List<SelectListItem>();
            this.ShippingAddress = new AddressModel();
        }
        [UrsDisplayName("Admin.Orders.Shipments.ID")]
        public override int Id { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.OrderID")]
        public int OrderId { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.TotalWeight")]
        public string TotalWeight { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.ShippingMethod")]
        public string ShippingMethod { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.TrackingNumber")]
        public string TrackingNumber { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.ShippedDate")]
        public string ShippedDate { get; set; }
        public bool CanShip { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.DeliveryDate")]
        public string DeliveryDate { get; set; }
        public bool CanDeliver { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.ExpressName")]
        public string ExpressName { get; set; }
        public IList<SelectListItem> AvailableExpressNames { get; set; }
        public List<ShipmentOrderGoodsModel> Goodss { get; set; }

        public bool DisplayPdfPackagingSlip { get; set; }

        public AddressModel ShippingAddress { get; set; }

        #region Nested classes

        public partial class ShipmentOrderGoodsModel : BaseEntityModel
        {
            public int OrderGoodsId { get; set; }

            public int GoodsId { get; set; }

            public string FullGoodsName { get; set; }
            public string Sku { get; set; }
            public string AttributeInfo { get; set; }

            //weight of one item (goods variant)
            public string ItemWeight { get; set; }
            public string ItemDimensions { get; set; }

            public int QuantityToAdd { get; set; }
            public int QuantityOrdered { get; set; }
            public int QuantityInThisShipment { get; set; }
            public int QuantityInAllShipments { get; set; }
        }
        #endregion
    }
}