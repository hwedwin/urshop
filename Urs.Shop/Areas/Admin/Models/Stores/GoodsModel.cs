﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Urs.Admin.Models.Settings;
using Urs.Admin.Validators.Stores;
using Urs.Data.Domain.Common;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsValidator))]
    public partial class GoodsModel : BaseEntityModel
    {
        public GoodsModel()
        {
            AvailableBrands = new List<SelectListItem>();
            AvailableCategories = new List<SelectListItem>();
            GoodsSpecs = new List<CustomAttributeModel>();
            CategoryIds = new List<int>();

        }
        //picture thumbnail
        [UrsDisplayName("Admin.Store.Goods.Fields.PictureThumbnailUrl")]
        public string PictureThumbnailUrl { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Name")]
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.ShortDescription")]
        public string ShortDescription { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.FullDescription")]
        public string FullDescription { get; set; }

        public string canshu { get; set; }
        public string attr { get; set; }
        public string imgs { get; set; }
        public string attrtable { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.AdminComment")]

        public string AdminComment { get; set; }

        public string SeName { get; set; }
        [UrsDisplayName("Admin.Store.Goods.Fields.GoodsTags")]
        public string GoodsTags { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Sku")]
        public string Sku { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.ManufacturerPartNumber")]
        public string ManufacturerPartNumber { get; set; }
        //categories
        [UrsDisplayName("Admin.Store.Goods.Categories")]
        public List<int> CategoryIds { get; set; }
        public IList<SelectListItem> AvailableCategories { get; set; }
        //brands
        [UrsDisplayName("Admin.Store.Goods.Fields.Brands")]
        public int BrandId { get; set; }
        public IList<SelectListItem> AvailableBrands { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.IsShipEnabled")]
        public bool IsShipEnabled { get; set; }

        [DisplayName("需要身份证")]
        public bool IsIDUpload { get; set; }
        [DisplayName("保税仓推送")]
        public bool PushSystem { get; set; }
        [UrsDisplayName("Admin.Store.Goods.Fields.IsFreeShipping")]
        public bool IsFreeShipping { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.AdditionalShippingCharge")]
        public decimal AdditionalShippingCharge { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.ManageInventoryMethod")]
        public bool ManageStockEnabled { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.StockQuantity")]
        public int StockQuantity { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.MinStockQuantity")]
        public int MinStockQuantity { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.LowStockActivity")]
        public int LowStockActivityId { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.NotifyAdminForQuantityBelow")]
        public int NotifyAdminForQuantityBelow { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.BackorderMode")]
        public int BackorderModeId { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.AllowBackInStockSubscriptions")]
        public bool AllowBackInStockSubscriptions { get; set; }
        [UrsDisplayName("Admin.Store.Goods.Fields.SaleVolume")]
        public int SaleVolume { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.AllowedQuantities")]
        public string AllowedQuantities { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.DisableBuyButton")]
        public bool DisableBuyButton { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.DisableWishlistButton")]
        public bool DisableWishlistButton { get; set; }
        [UrsDisplayName("Admin.Store.Goods.Fields.Unit")]
        public virtual string Unit { get; set; }
        [UrsDisplayName("Admin.Store.Goods.Fields.Price")]
        public decimal Price { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.OldPrice")]
        public decimal OldPrice { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.ProductCost")]
        public decimal ProductCost { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.SpecialPrice")]
        [UIHint("DecimalNullable")]
        public decimal? SpecialPrice { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.SpecialPriceStartDateTimeUtc")]
        [UIHint("DateNullable")]
        public DateTime? SpecialStartDateTimeUtc { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.SpecialPriceEndDateTimeUtc")]
        [UIHint("DateNullable")]
        public DateTime? SpecialEndDateTimeUtc { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Weight")]
        public decimal Weight { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Length")]
        public decimal Length { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Width")]
        public decimal Width { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Height")]
        public decimal Height { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.AvailableStartDateTime")]
        [UIHint("DateNullable")]
        public DateTime? AvailableStartDateTimeUtc { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.AvailableEndDateTime")]
        [UIHint("DateNullable")]
        public DateTime? AvailableEndDateTimeUtc { get; set; }

        public string BaseDimensionIn { get; set; }
        public string BaseWeightIn { get; set; }

        public bool Published { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.IconTip")]
        public string IconTip { get; set; }
        [UrsDisplayName("Admin.Store.Goods.Fields.ServiceTip")]
        public string ServiceTip { get; set; }

        //goods attributes
        public IList<CustomAttributeModel> GoodsSpecs { get; set; }

        public partial class CustomAttributeModel : BaseEntityModel
        {
            public CustomAttributeModel()
            {
                Values = new List<CustomAttributeValueModel>();
            }

            public string Name { get; set; }

            public bool IsRequired { get; set; }

            /// <summary>
            /// Default value for textboxes
            /// </summary>
            public string DefaultValue { get; set; }

            public IList<CustomAttributeValueModel> Values { get; set; }
        }

        public partial class CustomAttributeValueModel : BaseEntityModel
        {
            public string Name { get; set; }

            public bool IsPreSelected { get; set; }
        }


    }

}