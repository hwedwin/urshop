﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Admin.Models.Users;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Kendoui;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(CategoryValidator))]
    public partial class CategoryModel : BaseEntityModel
    {
        public CategoryModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }
            ParentCategories = new List<SelectListItem>();
        }

        [UrsDisplayName("Admin.Store.Categories.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.Description")]
        public string Description { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.SeName")]
        public string SeName { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.Parent")]
        public int ParentCategoryId { get; set; }

        [UIHint("Picture")]
        [UrsDisplayName("Admin.Store.Categories.Fields.Picture")]
        public int PictureId { get; set; }

        [UrsDisplayName("Admin.Store.Categories.Fields.PageSize")]
        public int PageSize { get; set; }
        [UrsDisplayName("Admin.Store.Categories.Fields.PriceRanges")]
        
        public string PriceRanges { get; set; }
        [UrsDisplayName("Admin.Store.Categories.Fields.ShowOnHomePage")]
        public bool ShowOnHomePage { get; set; }
        [UrsDisplayName("Admin.Store.Categories.Fields.Published")]
        public bool Published { get; set; }
        [UrsDisplayName("Admin.Store.Categories.Fields.Deleted")]
        public bool Deleted { get; set; }
        [UrsDisplayName("Admin.Store.Categories.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        public string Breadcrumb { get; set; }
        public IList<SelectListItem> ParentCategories { get; set; }

        #region Nested classes

        public partial class CategoryGoodsModel : BaseEntityModel
        {
            public int CategoryId { get; set; }

            public int GoodsId { get; set; }

            [UrsDisplayName("Admin.Store.Categories.Goodss.Fields.Goods")]
            public string GoodsName { get; set; }

            [UrsDisplayName("Admin.Store.Categories.Goodss.Fields.IsFeaturedGoods")]
            public bool IsFeaturedGoods { get; set; }

            [UrsDisplayName("Admin.Store.Categories.Goodss.Fields.DisplayOrder")]
            //we don't name it DisplayOrder because Telerik has a small bug 
            //"if we have one more editor with the same name on a page, it doesn't allow editing"
            //in our case it's category.DisplayOrder
            public int DisplayOrder1 { get; set; }
        }

        public partial class AddCategoryGoodsModel : BaseModel
        {
            public AddCategoryGoodsModel()
            {
                AvailableCategories = new List<SelectListItem>();
                AvailableBrands = new List<SelectListItem>();
            }
            public ResponseResult Goodss { get; set; }

            [UrsDisplayName("Admin.Store.Goods.List.SearchGoodsName")]
            
            public string SearchGoodsName { get; set; }
            [UrsDisplayName("Admin.Store.Goods.List.SearchCategory")]
            public int SearchCategoryId { get; set; }
            [UrsDisplayName("Admin.Store.Goods.List.SearchBrand")]
            public int SearchBrandId { get; set; }

            public IList<SelectListItem> AvailableCategories { get; set; }
            public IList<SelectListItem> AvailableBrands { get; set; }

            public int CategoryId { get; set; }

            public int[] SelectedGoodsIds { get; set; }
        }

        #endregion
    }

}