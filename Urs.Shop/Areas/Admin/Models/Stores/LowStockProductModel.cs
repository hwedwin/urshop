﻿using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    public partial class LowStockGoodsModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Store.Goods.Fields.Name")]
        public string Name { get; set; }
        public string Attributes { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.ManageInventoryMethod")]
        public bool ManageInventoryMethod { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.StockQuantity")]
        public int StockQuantity { get; set; }

        [UrsDisplayName("Admin.Store.Goods.Fields.Published")]
        public bool Published { get; set; }
    }
}