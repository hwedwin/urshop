﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Stores
{
    public partial class GoodsListModel : BaseModel
    {
        public GoodsListModel()
        {
            AvailableCategories = new List<SelectListItem>();
            AvailableBrands = new List<SelectListItem>();
            PublishedStatus= new List<SelectListItem>();
        }
        [UrsDisplayName("Admin.Store.Goods.List.SearchGoodsName")]
        public string SearchName { get; set; }
        public string SearchSku { get; set; }
        [UrsDisplayName("Admin.Store.Goods.List.SearchCategory")]
        public int SearchCategoryId { get; set; }
        [UrsDisplayName("Admin.Store.Goods.List.SearchBrand")]
        public int SearchBrandId { get; set; }
        public IList<SelectListItem> AvailableCategories { get; set; }
        public IList<SelectListItem> AvailableBrands { get; set; }
        public IList<SelectListItem> PublishedStatus { get; set; }
        [UrsDisplayName("Admin.Store.Goods.List.PublishedId")]
        public int PublishedId { get; set; }
    }
}