﻿using FluentValidation.Attributes;
using Urs.Admin.Validators.Settings;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    [Validator(typeof(PointsSettingsValidator))]
    public partial class PointsSettingsModel : BaseModel, ISettingsModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.RewardPoints.Enabled")]
        public bool Enabled { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.RewardPoints.ExchangeRate")]
        public decimal ExchangeRate { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.RewardPoints.PointsForRegistration")]
        public int PointsForRegistration { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Amount")]
        public decimal PointsForPurchases_Amount { get; set; }

        public int PointsForPurchases_Points { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Awarded")]
        public int PointsForPurchases_Awarded { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Canceled")]
        public int PointsForPurchases_Canceled { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }
    }
}