﻿using System;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class OnlineUserModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Users.OnlineUsers.Fields.UserInfo")]
        public string UserInfo { get; set; }

        [UrsDisplayName("Admin.Users.OnlineUsers.Fields.IPAddress")]
        public string LastIpAddress { get; set; }

        [UrsDisplayName("Admin.Users.OnlineUsers.Fields.Location")]
        public string Location { get; set; }

        [UrsDisplayName("Admin.Users.OnlineUsers.Fields.LastActivityDate")]
        public DateTime LastActivityDate { get; set; }
    }
}