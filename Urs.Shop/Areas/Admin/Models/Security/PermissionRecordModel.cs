﻿using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Security
{
    public partial class PermissionRecordModel : BaseModel
    {
        public string Name { get; set; }
        public string SystemName { get; set; }
    }
}