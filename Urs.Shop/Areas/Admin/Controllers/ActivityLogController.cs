﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using Urs.Admin.Models.Logging;
using Urs.Services.Logging;
using Urs.Services.Security;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Framework.Kendoui;

namespace Urs.Admin.Controllers
{
    [AdminAuthorize]
    public partial class ActivityLogController : BaseAdminController
    {
        #region Fields

        private readonly IActivityLogService _activityLogService;
        private readonly IPermissionService _permissionService;

        #endregion Fields

        #region Constructors

        public ActivityLogController(IActivityLogService activityLogService,
            IPermissionService permissionService)
        {
            this._activityLogService = activityLogService;
            this._permissionService = permissionService;
        }

        #endregion

        #region Activity log

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return HttpUnauthorized();

            var activityLogSearchModel = new ActivityLogSearchModel();
            activityLogSearchModel.ActivityLogType.Add(new SelectListItem()
            {
                Value = "0",
                Text = "All"
            });


            foreach (var at in _activityLogService.GetAllActivityTypes()
                .OrderBy(x => x.Name)
                .Select(x =>
                {
                    return new SelectListItem()
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    };
                }))
                activityLogSearchModel.ActivityLogType.Add(at);
            return View(activityLogSearchModel);
        }

        [HttpPost]
        public JsonResult ListJson(PageRequest command, ActivityLogSearchModel searchModel)
        {
            DateTime? startDateValue = (searchModel.CreateTimeFrom == null) ? null
                : (DateTime?)searchModel.CreateTimeFrom.Value;

            DateTime? endDateValue = (searchModel.CreateTimeTo == null) ? null
                            : (DateTime?)searchModel.CreateTimeTo.Value.AddDays(1);

            var activityLog = _activityLogService.GetAllActivities(startDateValue, endDateValue, null, searchModel.ActivityLogTypeId, command.Page - 1, command.Limit);

            var model = activityLog.Select(x =>
              {
                  var m = x.ToModel<ActivityLogModel>();
                  m.CreateTime = x.CreateTime;
                  return m;

              });

            var result = new ResponseResult
            {
                data = model,
                count = activityLog.TotalCount
            };
            return Json(result);
        }


        public IActionResult Delete(int id, PageRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return HttpUnauthorized();

            var activityLog = _activityLogService.GetActivityById(id);
            if (activityLog == null) return Json(new { error = 1 });

            _activityLogService.DeleteActivity(activityLog);

            return Json(new { success = 1 });
        }

        public IActionResult ClearAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return HttpUnauthorized();

            _activityLogService.ClearAllActivities();

            return Json(new { success = 1 });
        }

        #endregion

    }
}
