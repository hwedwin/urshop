﻿using FluentValidation;
using Urs.Admin.Models.Stores;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Stores
{
    public class GoodsReviewValidator : BaseUrsValidator<GoodsReviewModel>
    {
        public GoodsReviewValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage(localizationService.GetResource("Admin.Store.GoodsReviews.Fields.Title.Required"));
            RuleFor(x => x.ReviewText).NotEmpty().WithMessage(localizationService.GetResource("Admin.Store.GoodsReviews.Fields.ReviewText.Required"));
        }}
}