﻿using FluentValidation;
using Urs.Admin.Models.Stores;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Stores
{
    public class GoodsSpecValidator : BaseUrsValidator<GoodsSpecModel>
    {
        public GoodsSpecValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotNull().WithMessage(localizationService.GetResource("Admin.Store.GoodsSpecs.Fields.Name.Required"));
        }
    }
}