﻿using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UEditorNetCore;
using Urs.Framework.Infrastructure.Extensions;

namespace Urs.Web
{
    /// <summary>
    /// Represents startup class of application
    /// </summary>
    public class Startup
    {
        #region Properties

        /// <summary>
        /// Get Configuration of the application
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion

        #region Ctor

        public Startup(IConfiguration configuration)
        {
            //set configuration
            Configuration = configuration;
        }

        #endregion

        /// <summary>
        /// Add services to the application and configure service provider
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            string[] urls = Configuration.GetSection("AllowCors:AllowAllOrigin").Value.Split(',');
            services.AddCors(options =>
            {
                //options.AddPolicy("AllowAllOrigin", builder =>
                // {
                //     builder.WithOrigins(urls)
                //     .AllowAnyMethod()
                //     .AllowAnyHeader()
                //     .AllowCredentials();
                // });
                options.AddPolicy("any", builder =>
                {
                    builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .AllowCredentials();
                });
            });
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            services.AddUEditorService();
            return services.ConfigureApplicationServices(Configuration);
        }

        /// <summary>
        /// Configure the application HTTP request pipeline
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            //application.UseCors("AllowAllOrigin");
            application.UseCors("any");
            application.ConfigureRequestPipeline();
        }
    }
}
