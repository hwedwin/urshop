import api from '../../api/api'
import { topic } from '../../api/conf'
var WxParse = require('../../wxParse/wxParse.js')
Page({
  data: {
    
  },
  onLoad: function (options) {
    var that = this
    api.get(topic,{
      systemName: options.name
    }).then(res=>{
      wx.setNavigationBarTitle({
        title: res.Data.Title
      })
      var article = res.Data.Body;
      WxParse.wxParse('article', 'html', article, that, 5);
    })
  },
  onReady: function () {
    
  }
})