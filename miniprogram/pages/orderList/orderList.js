import api from '../../api/api'
import { orderList } from '../../api/conf'
import { ordercancel } from '../../api/conf'
import { paymentweixinwap } from '../../api/conf'
import { aftersales } from '../../api/conf'
Page({

  data: {
    hideText: '暂时没有退货订单',
    orderList: [],
    reTurn: false,
    page: 1,
    status: null,
    requests: []
  },
  onLoad: function (options) {
    this.setData({
      status: options.status
    })
    if (options.status == 10){
      wx.setNavigationBarTitle({
        title: '待付款'
      })
    } else if (options.status == 20){
      wx.setNavigationBarTitle({
        title: '待配送'
      })
    } else if (options.status == 23){
      wx.setNavigationBarTitle({
        title: '待提货'
      })
    } else if (options.status == 30){
      wx.setNavigationBarTitle({
        title: '已提货'
      })
    }
  },
  onReady: function () {

  },
  onShow: function(){
    this.getaftersales()
  },
  getOrderList: function () {
    var that = this
    if (this.data.reTurn) {
      return
    }
    wx.showNavigationBarLoading()
    api.get(orderList, {
      orderstatus: that.data.status,
      page: that.data.page
    }).then(res => {
      that.setData({
        orderList: that.data.orderList.concat(res.Data.Items)
      })
      if (res.Data.Items.length < 12) {
        that.setData({
          reTurn: true
        })
        wx.showToast({
          title: '没有更多了',
          icon: 'none'
        })
      }
      wx.hideNavigationBarLoading()
    })
  },
  lower: function (e) {
    if (this.data.reTurn){
      return
    }
    this.setData({
      page: this.data.page + 1
    })
    // this.getOrderList()
    this.getaftersales()
  },
  saleClick: function (e) {
    let orderId = e.currentTarget.dataset.orderid
    wx.navigateTo({
      url: '/pages/salePage/salePage?orderid=' + orderId,
    })
  },
  openConfirm: function (e) {
    var that = this
    wx.showModal({
      title: '提示',
      content: '确定取消订单？',
      success: function (res) {
        if (res.confirm) {
          api.post(ordercancel, {
            CustomerGuid: wx.getStorageSync('guid'),
            OrderGuid: e.currentTarget.dataset.orderguid
          }).then(res => {
            wx.showToast({
              title: '订单取消成功'
            })
            that.setData({
              reTurn: false,
              page: 1,
              orderList: []
            })
            that.getOrderList()
          }).catch(err => {
            wx.hideNavigationBarLoading()
            wx.showToast({
              title: '订单取消失败',
              icon: 'none'
            })
          })
        } else {

        }
      }
    });
  },
  pay: function (e) {
    wx.showLoading({
      title: '支付中',
    })
    api.get(paymentweixinwap, {
      orderId: e.currentTarget.dataset.orderid,
      paytype: 'Order'
    }).then(res => {
      wx.hideLoading()
      if (res.Code == 200) {
        let rep = res.Data.KeyValue
        wx.requestPayment({
          timeStamp: rep.timeStamp,
          nonceStr: rep.nonceStr,
          package: rep.package,
          signType: rep.signType,
          paySign: rep.paySign,
          success(res) {
            wx.showToast({
              title: '支付成功！',
              icon: 'success'
            })
            wx.switchTab({
              url: '/pages/my/my'
            })
          },
          fail(res) {
            wx.showToast({
              title: '支付失败！',
              icon: 'success'
            })
          }
        })
      }
    }).catch(err => {
      wx.showToast({
        title: '支付失败',
        icon: 'none'
      })
    })
  },
  getaftersales: function(){
    var that = this
    if (this.data.reTurn) {
      return
    }
    wx.showNavigationBarLoading()    
    api.get(aftersales,{
      page: that.data.page,
      size: 12
    }).then(res=>{
      that.setData({
        requests: res.Data.Items
      })
      wx.hideNavigationBarLoading()
      if (res.Data.Items.length < 12) {
        that.setData({
          reTurn: true
        })
        wx.showToast({
          title: '没有更多了',
          icon: 'none'
        })
      }
    })
  }
})