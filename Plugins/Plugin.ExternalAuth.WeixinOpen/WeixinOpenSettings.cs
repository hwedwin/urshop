using Urs.Core.Configuration;

namespace ExternalAuth.WeixinOpen
{
    /// <summary>
    /// Represents a Weixin wap payment settings
    /// </summary>
    public class WeixinOpenSettings : ISettings
    {
        /// <summary>
        /// 小程序的APPID（必须配置）
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 小程序的APPSecret
        /// </summary>
        public string AppSecret { get; set; }
    }
}