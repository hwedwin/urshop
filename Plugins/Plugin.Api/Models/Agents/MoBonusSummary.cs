﻿namespace Urs.Plugin.Misc.Api.Models.Agents
{
    /// <summary>
    /// 代理信息
    /// </summary>
    public partial class MoBonusSummary
    {
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 累积总分红
        /// </summary>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 可提现分红
        /// </summary>
        public decimal AvailAmount { get; set; }
        /// <summary>
        /// 待提现分红
        /// </summary>
        public decimal FreezingAmount { get; set; }
        /// <summary>
        /// 无效分红
        /// </summary>
        public decimal NoValidAmount { get; set; }
        /// <summary>
        /// 已提现分红
        /// </summary>
        public decimal CashAmount { get; set; }


    }
}