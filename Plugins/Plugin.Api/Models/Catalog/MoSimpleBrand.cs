﻿using Plugin.Api.Models.Media;

namespace Plugin.Api.Models.Catalog
{
    /// <summary>
    /// 品牌制造商
    /// </summary>
    public partial class MoSimpleBrand
    {
        public MoSimpleBrand()
        {
            Picture = new MoPicture();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; set; }
        /// <summary>
        /// 品牌制造商url
        /// </summary>
        public string SeName { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public MoPicture Picture { get; set; }
    }
}