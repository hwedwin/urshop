﻿using System.Collections.Generic;
using Urs.Data.Domain.Common;

namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public partial class MoInfo
    {
        public MoInfo()
        {
            Fields = new List<MoCustomField>();
        }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户Guid
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 角色数组
        /// </summary>
        public string RoleIdArray { get; set; }
        /// <summary>
        /// 用户昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 用户等级
        /// </summary>
        public MoLevel Level { get; set; }
        /// <summary>
        /// email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 推荐人Code
        /// </summary>
        public string ReferrerCode { get; set; }
        /// <summary>
        /// 推荐人昵称
        /// </summary>
        public string ReferrerNickname { get; set; }
        /// <summary>
        /// 推荐人头像
        /// </summary>
        public string ReferrerAvatarUrl { get; set; }
        /// <summary>
        /// 头像Id
        /// </summary>
        public int AvatarPictureId { get; set; }
        /// <summary>
        /// 头像url
        /// </summary>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public string BirthDate { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string StreetAddress { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string Zip { get; set; }
        /// <summary>
        /// 省份Id
        /// </summary>
        public int ProvinceId { get; set; }
        /// <summary>
        /// 省份名
        /// </summary>
        public string ProvinceName { get; set; }
        /// <summary>
        /// 城市Id
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 城市名
        /// </summary>
        public string CityName { get; set; }
        /// <summary>
        /// 区域Id
        /// </summary>
        public int AreaId { get; set; }
        /// <summary>
        /// 区县名
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// 积分
        /// </summary>
        public int Points { get; set; }
        /// <summary>
        /// 自定义字段
        /// </summary>
        public IList<MoCustomField> Fields { get; set; }
        public partial class MoLevel
        {
            /// <summary>
            /// 等级Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 等级编号
            /// </summary>
            public int Grade { get; set; }
            /// <summary>
            /// 等级名称
            /// </summary>
            public string Name { get; set; }
        }
        public partial class MoCustomField
        {
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 默认值
            /// </summary>
            public string DefaultValue { get; set; }
        }
    }
}