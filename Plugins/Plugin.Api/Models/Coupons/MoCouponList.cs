﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;

namespace Plugin.Api.Models.Coupons
{
    /// <summary>
    /// 优惠券列表
    /// </summary>
    public partial class MoCouponList
    {
        /// <summary>
        /// 优惠券列表
        /// </summary>
        public MoCouponList()
        {
            Items = new List<MoCoupon>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 优惠券列表
        /// </summary>
        public IList<MoCoupon> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }
    }
}