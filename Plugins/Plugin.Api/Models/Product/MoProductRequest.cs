﻿using System.Collections.Generic;
using Plugin.Api.Models.ShoppingCart;

namespace Plugin.Api.Models.Goods
{
    /// <summary>
    /// 商品详情
    /// </summary>
    public partial class MoGoodsRequest
    {
        public MoGoodsRequest()
        {
            PictureIds = new List<int>();
            Form = new List<MoKeyValue>();
            Tags = new List<string>();
        }
        /// <summary>
        /// 分类Id
        /// </summary>
        public int CategoryId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Short { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Full { get; set; }
        /// <summary>
        /// 商城价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 是否发布上架
        /// </summary>
        public bool Published { get; set; }
        /// <summary>
        /// 商品图片
        /// </summary>
        public IList<int> PictureIds { get; set; }
        /// <summary>
        /// 商品标签
        /// </summary>
        public IList<string> Tags { get; set; }
        /// <summary>
        /// 提交自定义字段
        /// </summary>
        public IList<MoKeyValue> Form { get; set; }
    }
}