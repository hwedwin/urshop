﻿using System;
using Urs.Core;
using Urs.Data.Domain.Users;

namespace Urs.Data.Domain.Coupons
{
    /// <summary>
    /// 优惠价用户映射
    /// </summary>
    public partial  class CouponUserMapping:BaseEntity
    {
       /// <summary>
       /// 客户id
       /// </summary>
       public virtual int UserId { get; set; }
       /// <summary>
       /// 优惠券Id
       /// </summary>
       public virtual int CouponId { get; set; }
       /// <summary>
       /// 是否已经使用
       /// </summary>
       public virtual bool IsUsed { get; set; }
       /// <summary>
       /// 使用时间
       /// </summary>
       public virtual DateTime? UsedTime { get; set; }
       /// <summary>
       /// 兑换时间
       /// </summary>
       public virtual DateTime CreateTime { get; set; }
       public virtual User User { get; set; }
       public virtual Coupon Coupon { get; set; }
    }
}
