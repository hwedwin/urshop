﻿using System.Collections.Generic;
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class SecuritySettings : ISettings
    {
        public bool ForceSslForAllPages { get; set; }
        /// <summary>
        /// Gets or sets an encryption key
        /// </summary>
        public string EncryptionKey { get; set; }

        public string PluginStaticFileExtensionsBlacklist { get; set; }
        /// <summary>
        /// Gets or sets a list of adminn area allowed IP addresses
        /// </summary>
        public List<string> AdminAreaAllowedIpAddresses { get; set; }
        /// <summary>
        /// Gets or sets a vaule indicating whether to hide admin menu items based on ACL
        /// </summary>
        public bool HideAdminMenuItemsBasedOnPermissions { get; set; }
        public bool AllowNonAsciiCharactersInHeaders { get; set; }
    }
}