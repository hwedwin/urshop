
namespace Urs.Data.Domain.Users
{
    public static partial class SystemRoleNames
    {
        public static string Administrators { get { return "Administrators"; } }

        public static string Operators { get { return "Operators"; } }

        public static string Registered { get { return "Registered"; } }

        public static string Guests { get { return "Guests"; } }

        public static string Agents { get { return "Agents"; } }


    }
}