using Urs.Core;
using System;
using System.Collections.Generic;

namespace Urs.Data.Domain.Stores
{
    public partial class Goods : BaseEntity
    {
        private ICollection<GoodsTagMapping> _goodsTags;
        private ICollection<GoodsSpecCombination> _goodsSpecCombinations;

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        ///Sku
        /// </summary>
        public virtual string Sku { get; set; }
        /// <summary>
        /// 制造编号
        /// </summary>
        public virtual string ManufacturerPartNumber { get; set; }
        /// <summary>
        /// 简单描述
        /// </summary>
        public virtual string ShortDescription { get; set; }

        /// <summary>
        /// 全文内容
        /// </summary>
        public virtual string FullDescription { get; set; }

        /// <summary>
        /// 管理端备注
        /// </summary>
        public virtual string AdminComment { get; set; }

        /// <summary>
        /// 首页显示
        /// </summary>
        public virtual bool ShowOnHomePage { get; set; }
        /// <summary>
        /// 是否需要配送
        /// </summary>
        public virtual bool IsShipEnabled { get; set; }
        /// <summary>
        /// 免费配送
        /// </summary>
        public virtual bool IsFreeShipping { get; set; }
        /// <summary>
        /// 是否需要额外配送费
        /// </summary>
        public virtual decimal AdditionalShippingCharge { get; set; }
        /// <summary>
        /// 是否启用库存
        /// </summary>
        public virtual bool ManageStockEnabled { get; set; }
        /// <summary>
        /// 库存
        /// </summary>
        public virtual int StockQuantity { get; set; }
        /// <summary>
        /// 自定义属性
        /// </summary>
        public virtual string AttributesXml { get; set; }
        /// <summary>
        /// 最低购买库存
        /// </summary>
        public virtual int MinStockQuantity { get; set; }
        /// <summary>
        /// 销量
        /// </summary>
        public virtual int SaleVolume { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public virtual decimal Price { get; set; }
        /// <summary>
        /// 市面价
        /// </summary>
        public virtual decimal OldPrice { get; set; }
        /// <summary>
        /// 成本价
        /// </summary>
        public virtual decimal ProductCost { get; set; }
        /// <summary>
        /// 促销价
        /// </summary>
        public virtual decimal? SpecialPrice { get; set; }
        /// <summary>
        /// 促销价开始时间
        /// </summary>
        public virtual DateTime? SpecialStartTime { get; set; }
        /// <summary>
        /// 促销价结束时间
        /// </summary>
        public virtual DateTime? SpecialEndTime { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        public virtual decimal Weight { get; set; }
        /// <summary>
        /// 长度
        /// </summary>
        public virtual decimal Length { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public virtual decimal Width { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        public virtual decimal Height { get; set; }

        public virtual int CategoryId { get; set; }

        public virtual int BrandId { get; set; }
        /// <summary>
        /// 评分
        /// </summary>
        public virtual int ApprovedRatingSum { get; set; }
        /// <summary>
        /// 评价数
        /// </summary>
        public virtual int ApprovedTotalReviews { get; set; }
        /// <summary>
        /// 发布上架
        /// </summary>
        public virtual bool Published { get; set; }
        /// <summary>
        /// 删除
        /// </summary>
        public virtual bool Deleted { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public virtual DateTime UpdateTime { get; set; }

        public virtual ICollection<GoodsSpecCombination> GoodsSpecCombinations
        {
            get { return _goodsSpecCombinations ?? (_goodsSpecCombinations = new List<GoodsSpecCombination>()); }
            protected set { _goodsSpecCombinations = value; }
        }

        public virtual ICollection<GoodsTagMapping> GoodsTags
        {
            get { return _goodsTags ?? (_goodsTags = new List<GoodsTagMapping>()); }
            protected set { _goodsTags = value; }
        }


    }
}