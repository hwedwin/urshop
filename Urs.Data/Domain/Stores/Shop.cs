﻿using System;
using Urs.Core;

namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 电商企业
    /// </summary>
    public class Shop : BaseEntity
    {
        /// <summary>
        /// 电商编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 电商名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public decimal Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public decimal Longitude { get; set; }
        /// <summary>
        /// 营业时间
        /// </summary>
        public string OpeningHours { get; set; }
        /// <summary>
        /// 发布上架
        /// </summary>
        public virtual bool Published { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
