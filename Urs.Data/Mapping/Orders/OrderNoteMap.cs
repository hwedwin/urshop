
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Mapping.Orders
{
    public partial class OrderNoteMap : UrsEntityTypeConfiguration<OrderNote>
    {
        public override void Configure(EntityTypeBuilder<OrderNote> builder)
        {
            builder.ToTable(nameof(OrderNote));
            builder.HasKey(on => on.Id);
            builder.Property(on => on.Note).IsRequired();

            builder.HasOne(on => on.Order)
                .WithMany(o => o.OrderNotes)
                .HasForeignKey(on => on.OrderId);
            base.Configure(builder);
        }
    }
}