
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Users;

namespace Urs.Data.Mapping.Users
{
    public partial class ExternalAuthMap : UrsEntityTypeConfiguration<ExternalAuth>
    {
        public override void Configure(EntityTypeBuilder<ExternalAuth> builder)
        {
            builder.ToTable(nameof(ExternalAuth));

            builder.HasKey(ear => ear.Id);
            builder.Property(ear => ear.AvatarUrl);
            builder.Property(ear => ear.OpenId);
            builder.Property(ear => ear.OAuthToken);
            builder.Property(ear => ear.OAuthRefreshToken);
            builder.Property(ear => ear.ProviderSystemName);

            builder.HasOne(ear => ear.User)
                .WithMany(c => c.ExternalAuthRecords)
                .HasForeignKey(ear => ear.UserId);

            base.Configure(builder);
        }
    }
}