
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Users;

namespace Urs.Data.Mapping.Users
{
    public partial class UserRoleMappingMap : UrsEntityTypeConfiguration<UserRoleMapping>
    {
        public override void Configure(EntityTypeBuilder<UserRoleMapping> builder)
        {
            builder.ToTable(nameof(UserRoleMapping));

            builder.HasKey(mapping => new { mapping.UserId, mapping.UserRoleId });

            builder.HasOne(mapping => mapping.User)
                .WithMany(user => user.UserRoleMappings)
                .HasForeignKey(mapping => mapping.UserId)
                .IsRequired();

            builder.HasOne(mapping => mapping.UserRole)
                .WithMany()
                .HasForeignKey(mapping => mapping.UserRoleId)
                .IsRequired();

            builder.Ignore(mapping => mapping.Id);

            base.Configure(builder);
        }
    }
}