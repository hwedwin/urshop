﻿

using Urs.Core.Data;
using Urs.Data.Domain.Common;
using Urs.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using Urs.Data.Extensions;
using System.Text;
using Urs.Data;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;

namespace Urs.Data
{
    public class MySqlDataProvider : IDataProvider
    {
        public void InitializeDatabase()
        {
            var context =  EngineContext.Current.Resolve<IDbContext>();
            //var tableNamesToValidate = new List<string> { "User", "Discount", "Order", "Goods", "ShoppingCartItem" };
            //var existingTableNames = context
            //    .QueryFromSql<StringQueryType>("SELECT table_name AS Value FROM INFORMATION_SCHEMA.TABLES WHERE table_type = 'BASE TABLE'")
            //    .Select(stringValue => stringValue.Value).ToList();
            //var createTables = !existingTableNames.Intersect(tableNamesToValidate, StringComparer.InvariantCultureIgnoreCase).Any();
            //if (!createTables)
            //    return;

            var fileProvider = EngineContext.Current.Resolve<IUrsFileProvider>();

            //create tables
            context.ExecuteSqlScript(context.GenerateCreateScript());

            //create indexes
            //context.ExecuteSqlScriptFromFile(fileProvider.MapPath("~/App_Data/MySql.Indexes.sql"));

            //create stored procedures 
            //context.ExecuteSqlScriptFromFile(fileProvider.MapPath("~/App_Data/MySql.StoredProcedures.sql"));
        }

        /// <summary>
        /// Gets a support database parameter object (used by stored procedures)   
        /// <returns>Parameter</returns>
        public virtual DbParameter GetParameter()
        {
            return new MySqlParameter();
        }

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this data provider supports backup
        /// </summary>
        public virtual bool BackupSupported => true;

        /// <summary>
        /// Gets a maximum length of the data for HASHBYTES functions, returns 0 if HASHBYTES function is not supported
        /// </summary>
        public virtual int SupportedLengthOfBinaryHash => 8000; //for SQL Server 2008 and above HASHBYTES function has a limit of 8000 characters.

        #endregion

    }
}